
require 'rubygems'
require 'curb'
require 'open-uri'
require 'easy_translate'
require 'json'

sdl_api_key = "ORAGEwxPj058TMNgAq4A6w%3D%3D"

google_api_key = "AIzaSyCrrGe5TDY55Z_YnVQ5rldiQ0qfWreIwlA"

text_to_translate = "For a very long time, fossils of palm trees were believed[by whom?] to be the oldest monocots, first appearing 90 million years ago, but this estimate may not be entirely true (reviewed in Herendeen and Crane, 1995[10]). At least some putative monocot fossils have been found in strata as old as the eudicots (reviewed in Herendeen et al., 1995[11]). The oldest fossils that are unequivocally monocots are pollen from the Late Barremian-Aptian - Early Cretaceous period, about 120-110 million years ago, and are assignable to clade-Pothoideae-Monstereae Araceae; being Araceae, sister to other Alismatales (Friis et al., 2004:[12]) for fossil monocots, see Gandolfo et al. 2000 [71] and Friis et al. 2006b [72]). They have also found flower fossils of Triuridaceae (Pandanales) in Upper Cretaceous rocks in New Jersey (Gandolfo et al. 2002 [73]), becoming the oldest known sighting of saprophytic /mycotrophic habits in angiosperm plants and among the oldest known fossils of monocotyledons.  Topology of the angiosperm phylogenetic tree could infer that the monocots would be among the oldest lineages of angiosperms, which would support the theory that they are just as old as the eudicots. The pollen of the eudicots dates back 125 million years, so the lineage of monocots should be that old too.  Molecular clock estimates for the age of extant monocots[edit]  Bremer (2000 [74] 2002 [75]), using rbcL sequences and the mean path length method ('mean-path lengths method'), estimated the age of the monocot crown group (i.e., the time at which the ancestor of today's Acorus diverged from the rest of the group) as 134 million years. Similarly, Wikström et al. (2001 [76]), using Sanderson’s (1997 [77]) non-parametric rate smoothing approach ('nonparametric rate smoothing approach'), obtained ages of 158 or 141 million years for the crown group of monocots (see Sanderson et al. 2004 [78]). All these estimates have large error ranges (usually 15-20%), and Wikström et al. [76] used only a single calibration point, namely the split between Fagales and Cucurbitales, which was set to 84 Ma, in the late Santonian period). Early molecular clock studies using strict clock models had estimated the monocot crown age to 200 ± 20 million years ago (Savard et al. 1994 [79]) or 160 ± 16 million years (Goremykin et al. 1997 [80]), while studies using relaxed clocks have obtained 135-131 million years (Leebens-Mack et al. 2005 [81]) or 133.8 to 124 million years (Moore et al. 2007 [82]). Bremer’s estimate (2000 [74]) of 134 million years has been used as a secondary calibration point in other analyses (Janssen and Bremer 2004 [83]).  Core group[edit]  The age of the core group of so-called ‘nuclear monocot’ or ‘core monocots’ by the Angiosperm Phylogeny Website ('core monocots' in English), which correspond to all orders except Acorales and Alismatales, is about 131 million years to present, and crown group age is about 126 million years to the present. The subsequent branching in this part of the tree (i.e., Petrosaviaceae, Dioscoreales + Pandanales and Liliales clades appeared), including the crown Petrosaviaceae group may be in the period around 125-120 million years BC (about 111 million years so far in Bremer 2000 [74]), and stem groups of all other orders, including Commelinadae would have diverged about or shortly after 115 million years (Janssen and Bremer 2004 [83]). These and many clades within these orders may have originated in southern Gondwana, i.e., Antarctica, Australasia, and southern South America (Bremer and Jansen 2006 [84]).  Aquatic monocots[edit]  The aquatic monocots of Alismatales have commonly been regarded as 'primitive' (Hallier, 1905, [85] Arber 1925, [86] Hutchinson, 1934, [87] Cronquist 1968, [88] 1981, [5] Takhtajan 1969, [89] 1991 [90] Stebbins 1974, [91] Thorne 1976 [92]). They have also been considered to have the most primitive foliage, which were cross-linked as Dioscoreales (Dahlgren et al.). 1985 [15] and Melanthiales (Thorne 1992a, [93] 1992b [94]). Keep in mind that, as stressed by Soltis et al. 2005, the 'most primitive' monocot is not necessarily 'the sister of everyone else.' This is because the ancestral or primitive characters are inferred by means of the reconstruction of characteristic states, with the help of the phylogenetic tree. So primitive characters of monocots may be present in some derived groups. On the other hand, the basal taxa may exhibit many morphological autapomorphies. So although Acoraceae is the sister group to the remaining monocotyledons, the result does not imply that Acoraceae is 'the most primitive monocot' in terms of its characteristics. In fact, Acoraceae is highly derived in most morphological characteristics, which is precisely why so many Alismatales Acoraceae occupied relatively imitative positions in trees produced by Chase et al. 1995b [14] and Stevenson and Loconte 1995. [50] (see section phylogeny).  Some authors support the idea of an aquatic phase as the origin of monocots (Henslow 1893, [95] and also cited and argued in the phylogeny section that Alismatales are the most primitive). The phylogenetic position of Alismatales (many water), which occupy a relationship with the rest except the Acoraceae, do not rule out the idea, because it could be ‘the most primitive monocots’ but not ‘the most basal’. The Atactostele stem, the long and linear leaves, the absence of secondary growth (see the biomechanics of living in the water), roots in groups instead of a single root branching (related to the nature of the substrate), including sympodial use, are consistent with a water source. However, while monocots were sisters of the aquatic Ceratophyllales, or their origin is related to the adoption of some form of aquatic habit, it would not help much to the understanding of how it evolved to develop their distinctive anatomical features: the monocots seem so different from the rest of angiosperms and it’s difficult to relate their morphology, anatomy and development and those of broad-leaved angiosperms (e.g. Zimmermann and Tomlinson 1972; [96] Tomlinson 1995 [22]).  Other taxa[edit]  In the past, taxa which had petiolate leaves with reticulate venation were considered 'primitive' within the monocots, because of its superficial resemblance to the leaves of dicotyledons. Recent work suggests that these taxa are sparse in the phylogenetic tree of monocots, such as fleshy fruited taxa (excluding taxa with aril seeds dispersed by ants), the two features would be adapted to conditions that evolved together regardless (Dahlgren and Clifford 1982; [97] Patterson and Givnish 2002, [98] Givnish et al. 2005b, [16] 2006b [99]). Among the taxa involved were Smilax, Trillium (Liliales), Dioscorea (Dioscoreales), etc. A number of these plants are vines that tend to live in shaded habitats for at least part of their lives, and may also have a relationship with their shapeless stomata (see Cameron and Dickison 1998 [100] for references on this last characteristic). Reticulate venation seems to have appeared at least 26 times in monocots, in fleshy fruits 21 times (sometimes lost later), and the two characteristics, though different, showed strong signs of a tendency to be good or bad in tandem, a phenomenon Givnish et al. (2005b, [16] 2006b [99]) described as “concerted convergence” (“coordinated convergence”)."

de_text = "Für eine sehr lange Zeit, Fossilien von Palmen waren[von wem?] die älteste monocots, die 90 Millionen Jahre her, aber diese Einschätzung möglicherweise nicht ganz richtig (geprüft in Herendeen und Kran, 1995 [ 10] ).  Zumindest einige vermeintliche lignifizierter Monokotylengewebe Fossilien gefunden wurden in Schichten so alt wie die eudicots (geprüft in Herendeen et al., 1995 [ 11] ).  Die ältesten Fossilien, die eindeutig monocots sind Pollen aus der späten Barremian-Aptian - Frühe Kreidezeit, etwa 120-110 Millionen Jahren, und sind frei belegbar, clade- Pothoideae-Monstereae Araceae; Araceae, der Schwester der anderen Alismatales (Friis et al., 2004: [ 12]) für fossile monocots, siehe Gandolfo et al. 2000 [ 71] und Friis et al. 2006b [ 72] ).  Sie haben auch gefunden Blume Fossilien Triuridaceae (Pandanales) in die obere Kreidezeit Rocks in New Jersey (Gandolfo et al. 2002 [ 73] ), indem sie die älteste bekannte Sichtung der saprophytischen /mycotrophic Gewohnheiten in Angiospermen pflanzen und zu den ältesten bekannten Fossilien von monocotyledons. Topologie der Angiospermen phylogenetischen Baum könnte daraus schließen, dass die monocots würden zu den ältesten Gilden der Angiospermen, zur Unterstützung der von der Theorie, dass sie sind nur so alt wie die eudicots. Die Pollen der eudicots geht zurück 125 Millionen Jahre, so dass die Linie der monocots sollte sein, dass viel zu alt. Molekulare Uhr schã¤tzungen FÃ¼r das Alter der bestehenden monocots[bearbeiten] Bremer (2000 [ 74] 2002 [ 75] ), und verwenden Sie hierzu rbcL Sequences und die mittlere Weglänge Methode ( \"mean-Weglängen Methode\" ), geschätzte alter des lignifizierter Monokotylengewebe Krone Gruppe (d. h. die Zeit, zu der die Vorfahren der heutigen Acorus wichen von dem Rest der Gruppe) als 134 Millionen Jahre alt sind. In ähnlicher Weise, Wikström et al. (2001 [ 76] ), mit Hilfe des Sanderson (1997 [ 77]) nicht-parametrische rate smoothing Ansatz ( \"nichtparametrische rate smoothing Ansatz\" ), im Alter von 158 oder 141 Millionen Jahren für die Krone Gruppe der monocots (siehe Sanderson et al. 2004 [ 78] ).  Alle diese Schätzungen haben große Fehler liegt (in der Regel 15-20 % ), Wikström et al. [ 76] verwendet nur einen einzigen Punkt, nämlich die Spaltung zwischen Fagales, Cucurbitales, gab 84 Ma, in den späten Santonian Zeitraum).  Frühe molekulare Uhr Studien mit strengen Takt Modelle geschätzt hatte die lignifizierter Monokotylengewebe Krone alter auf 200 ± 20 Millionen Jahren (Savard befindet et al. 1994 [ 79]) bzw. 160 ± 16 Millionen Jahre (Goremykin et al. 1997 [ 80] ), während die Studien mit entspannten Uhren erhalten haben 135-131 Millionen Jahren ( Leebens-Mack et al. 2005 [ 81]) oder 133,8 bis 124 Millionen Jahren (Moore et al. 2007 [ 82] ).  Bremer die Schätzung (2000 [ 74]) von 134 Millionen Jahren gemacht wurde, wird sie als sekundäre Kalibrierung Punkt in anderen Analysen (Janssen und Bremer 2004 [ 83] ).  Kern der Gruppe [Bearbeiten] das Alter der Kerngruppe des so genannten \"nukleare lignifizierter Monokotylengewebe' oder 'core monocots' von der Angiospermen Phylogeny Website ( \"core monocots' in Englisch), die entsprechen alle Bestellungen aus, außer Acorales Alismatales und, ist etwa 131 Millionen Jahre bis zur Gegenwart; und Krone Gruppe Alter ist ca. 126 Millionen Jahre bis in die Gegenwart. Die nachfolgende Verzweigungen in diesem Teil des Baums (d. h. , Petrosaviaceae, Dioscoreales Pandanales und Liliales clades erschienen), einschließlich der Krone Petrosaviaceae Gruppe kann sich in der Zeit um 125-120 Millionen Jahre v. Chr. (ca. 111 Millionen Jahre so weit im Bremer 2000 [ 74] ), und der Stiel sind Gruppen von allen anderen Aufträgen, einschließlich Commelinadae würde sich über oder kurz nach 115 Millionen Jahren (Janssen und Bremer 2004 [ 83] ).  Diese und viele clades in diese Bestellungen können haben ihren Ursprung im südlichen Gondwana, d. h. , Antarktis, Australien und Südamerika (Bremer und Jansen 2006 [ 84] ).  Aquatische monocots[bearbeiten] Die aquatische monocots der Alismatales sind häufig als \"primitiven\" (Hallier, 1905, [ 85] Arber 1925, [ 86] Hutchinson, 1934, [ 87] Cronquist 1968 [ 88] 1981 [ 5] Takhtajan 1969 [ 89] 1991 [ 90] Stebbins war 1974, [ 91] Thorne 1976 [ 92] ).  Sie wurden auch als die primitivsten Laub, die cross-linked als Dioscoreales (Dahlgren et al. ).  1985 [ 15] und Melanthiales (Thorne 1992a, [ 93] 1992b [ 94] ).  Denken Sie daran, dass, wie bereits von Soltis et al. 2005, Die \"primitivsten\" lignifizierter Monokotylengewebe ist nicht unbedingt \"die Schwester von allen anderen.\" Dieses ist, weil die Vorfahren oder primitiven Zeichen abgeleitet werden durch den Wiederaufbau von charakteristischen Staaten, mit der Hilfe des phylogenetischen Baum. So primitiv Zeichen der monocots vorhanden sein können in einigen Gruppen abgeleitet. Auf der anderen Seite, die basal Taxa aufweisen können viele morphologische autapomorphies. So Alth"

#request = Curl.get("https://www.googleapis.com/language/translate/v2?key=#{google_api_key}&source=en&target=de&q=#{URI::encode(mini_text)}")
#puts request.body_str

google_time_1 = Time.now
request = EasyTranslate.translate(text_to_translate, :to => :de, :key => google_api_key)
google_time_2 = Time.now
puts "Google Output"
puts request
puts "Google process time - #{google_time_2-google_time_1} seconds"

sdl_time_1 = Time.now
#curl -X POST -H "Content-type: application/json" -H "Authorization: LC apiKey=<YOUR API KEY>" -d
sdl_request = Curl.post "https://lc-api.sdl.com/translate", '{"text":"'+URI.encode(text_to_translate)+'", "from":"eng", "to":"ger"}' do |http|

  http.headers["Content-type"] = "application/json"
  http.headers["Authorization"] = "LC apiKey=#{sdl_api_key}"

end

sdl_time_2 = Time.now

puts "SDL Output"
puts sdl_request.body_str
puts "SDL process time - #{sdl_time_2-sdl_time_1} seconds"


# DE to Korean
google_time_3 = Time.now
request = EasyTranslate.translate(de_text, :to => :ko, :key => google_api_key)
google_time_4 = Time.now
puts "Google Output"
puts request
puts "Google process time - #{google_time_4-google_time_3} seconds"


sdl_time_3 = Time.now
#curl -X POST -H "Content-type: application/json" -H "Authorization: LC apiKey=<YOUR API KEY>" -d
sdl_request_1 = Curl.post "https://lc-api.sdl.com/translate", '{"text":"'+URI.encode(text_to_translate)+'", "from":"ger", "to":"eng"}' do |http|

  http.headers["Content-type"] = "application/json"
  http.headers["Authorization"] = "LC apiKey=#{sdl_api_key}"

end

sdl_response_1 = JSON.parse(sdl_request_1.body_str)

sdl_request_1 = Curl.post "https://lc-api.sdl.com/translate", '{"text":"'+URI.encode(sdl_response_1["translation"])+'", "from":"eng", "to":"kor"}' do |http|

  http.headers["Content-type"] = "application/json"
  http.headers["Authorization"] = "LC apiKey=#{sdl_api_key}"

end

sdl_time_4 = Time.now

puts "SDL Output"
puts sdl_request_1.body_str
puts "SDL process time - #{sdl_time_4-sdl_time_3} seconds"
